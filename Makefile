SCRIPTS = $(CURDIR)/scripts
ENV_LOCAL = $(CURDIR)/.pgenv_local
ENV_REMOTE = $(CURDIR)/.pgenv_remote
MAIN = $(SCRIPTS)/create_tables.sql
IMPORT = $(SCRIPTS)/insert_data.sql
SHELL := /usr/bin/env bash

all:
	@echo "Error: Please specify remote or local target!"	

remote:
	source $(ENV_REMOTE) && psql -h $${PGHOST} -d $${PGDATABASE} -U $${PGUSER} -f $(MAIN)
	source $(ENV_REMOTE) && psql -h $${PGHOST} -d $${PGDATABASE} -U $${PGUSER} -f $(IMPORT)
	
local:
	source $(ENV_LOCAL) && psql -h $${PGHOST} -d $${PGDATABASE} -U $${PGUSER} -f $(MAIN)
	source $(ENV_LOCAL) && psql -h $${PGHOST} -d $${PGDATABASE} -U $${PGUSER} -f $(IMPORT)
