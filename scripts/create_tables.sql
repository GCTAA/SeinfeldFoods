BEGIN TRANSACTION;
DROP TABLE IF EXISTS episodes;
DROP TABLE IF EXISTS foods;
DROP TABLE IF EXISTS foods_episodes;
DROP TABLE IF EXISTS food_types;
CREATE TABLE episodes (
  id integer primary key,
  season int,
  name text );
CREATE TABLE foods(
  id integer primary key,
  type_id integer,
  name text );
CREATE TABLE foods_episodes(
  food_id integer,
  episode_id integer );
CREATE TABLE food_types(
  id integer primary key,
  name text );
COMMIT;
