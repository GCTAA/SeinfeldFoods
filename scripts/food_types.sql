SELECT foods.name, food_types.name
FROM foods INNER JOIN food_types
ON foods.type_id = food_types.id;
